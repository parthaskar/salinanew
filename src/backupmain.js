// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import vueResource from 'vue-resource'
import {bus } from './bus'
import { store } from './store/store'
// import MainContent from './components/MainContent'

// export const bus = new Vue();

Vue.config.productionTip = true

Vue.use(vueResource)
Vue.use(VueRouter)


const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    // {path:'/',component: MainContent }
  ]
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})


// const bus = require('./bus.js');

var newtestdata = '';

var graphURL = 'http://localhost:3000/db';

var tableData = new Vue({
  store,
  data: {
    graphD:'',
    items: '',
    title: 'test title',
    comboChart: [
      { Date: "Januar", Categories: [{ Name: "Cat.1", Value: 368 }, { Name: "Cat.2", Value: 321 }], LineCategory: [{ Name: "Line1", Value: 69 }, { Name: "Line2", Value: 63 }] },
      { Date: "Februar", Categories: [{ Name: "Cat.1", Value: 521 }, { Name: "Cat.2", Value: 123 }], LineCategory: [{ Name: "Line1", Value: 89 }, { Name: "Line2", Value: 96 }] },
      { Date: "März", Categories: [{ Name: "Cat.1", Value: 368 }, { Name: "Cat.2", Value: 236 }], LineCategory: [{ Name: "Line1", Value: 63 }, { Name: "Line2", Value: 35 }] }
    ],

    linearChart: [
      { name: "Jan", value: 8009.28 },
      { name: "Feb", value: 7369.44 },
      { name: "Tot", value: 21855.36 }
    ]
  },

  methods: {
    graphData: function () {
    var self = this;
    var testdata= '';
    $.get( graphURL, function( data ) {
        self.items = data;
        testdata = data;
        console.log(testdata);
//////////////////
        new RadialProgressChart(".graph", {
      stroke: {
        width: 24,
        gap: 14
      },
      animation: {
        duration: -1,
        delay: -1
      },
      series: self.items.radialChart[0]
      });

/////////
new RadialProgressChart('.graph2', {
  // diameter: 42,
  stroke: {
    width: 24,
    gap: 14
  },
  animation: {
    duration: -1,
    delay: -1
  },
  // data is now from Vue object

  series:self.items.radialChart[1]
  }
)


    });
// $(document).data('mysite.option', 'blah');
    console.log(testdata);
    newtestdata = testdata;
  },

    fetchData: function (data) {
    this.graphD = data;
    // console.log('Inside', this.graphD);

  },

  getStore: function (){
    //  this.graphD = this.$store.state.storeGdata;
    //  console.log('Inside', this.graphD);
  }


  },

created: function() {
    this.graphData();
    this.fetchData();
},
  computed:{

  }



});



// console.log('OutSide', tableData._data.graphD)


// export var radialChart =[
//         [
//           {value: 60,
//           color: '#6aae6a'},
//           {value: 70,
//           color: '#356307'},
//           {value: 50,
//           color: '#005128'}
//         ],
//         [
//           {value: 80, color: '#6aae6a'},
//           {value: 90, color: '#356307'},
//           {value: 90, color: '#005128'}
//         ]
//     ]





// new RadialProgressChart('.graph', {
//   // diameter: 42,
//   stroke: {
//     width: 24,
//     gap: 14
//   },
//   animation: {
//     duration: -1,
//     delay: -1
//   },
//   // data is now from Vue object
//
//   // series:
//   series:tableData._data.radialChart[1]
//
// }
// )




////// Linear Bar

var title = "Umsatz";
// color management
// var colorPicker = d3.scaleQuantize()
//     .domain([0, 1])
//     .range(["#005128", "#006532", "#00783b", "#008c45", "#009f4f", "#6aae6a"]);

var colorPicker = ["#005128", "#006532", "#00783b", "#008c45", "#009f4f", "#6aae6a"];

var dataset = (tableData) ? tableData._data.linearChart : undefined ;
var max = d3.max(dataset, function(d){ return d.value; });
var currency = "CHF";
var dest = "#rightLowerGraph";

// add the title
var h4 = document.createElement('h4');
h4.innerText = title;
document.querySelector(dest).append(h4);

// Loop through the elements and render those
for(var i = 0, ln = dataset.length; i < ln; i++) {
  // Captions first
  var div = document.createElement('div');
  div.className = "col-xs-12";
  // left part
  var left = document.createElement("span");
  left.innerText = dataset[i].name+":";
  div.append(left);
  // right part
  var pickedColor = colorPicker[Math.round((dataset[i].value/max)*(colorPicker.length-1))];
  var right = document.createElement("span");
  right.style.float = "right";
  right.innerHTML = currency + " " + "<span style='color:"+pickedColor+"'>"+dataset[i].value+"</span>";
  div.append(right);
  // append
  document.querySelector(dest).append(div);
  // Make an SVG Container
  var svg = d3.select(dest).append("svg")
                                      .attr("width", "100%")
                                      .attr("height", 22);
  // Draw the Rectangle
  var rectangle = svg.append("rect")
                              .attr("x", 0)
                              .attr("y", 0)
                              .attr("rx", 10)
                              .attr("ry", 30)
                              .attr("width", (100*dataset[i].value/max)+"%")
                              .attr("height", 22)
                              .attr("fill", pickedColor);
  //
  document.querySelector(dest).append(document.createElement("br"));
}

//////////////
