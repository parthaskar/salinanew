import ContentArea from './components/ContentArea'
import GraphComparison from './components/GraphComparison'


export const routes =[
    { path:'', component: ContentArea},
    { path:'/combo/', component: GraphComparison }
  ];
