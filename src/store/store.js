import Vue from 'vue';
import Vuex from 'vuex';

Vue.use (Vuex);

export const store = new Vuex.Store ( {
  state: {
     globalurl:'http://172.16.1.157:8002',
     storeGdata:'',
     navdata:'',
     // Home Graph Data
     perf1:'',
     perf2:'',
     perf3:'',
     bud1:'',
     bud2:'',
     bud3:'',
     um1:'',
     um2:'',
     um3:''
  }
}

);
