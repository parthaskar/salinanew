# salina

> salina dashboard

## Build Setup

### clone project
```
git clone ssh://git@ssh.git.avionix.ee:7999/sal/salina-client.git
```
### get into salina-client
```
cd salina-client
```
### install dependencies
```
npm install

npm install vue-router vue-resource --save

npm install vue-awesome --save
```
### serve with hot reload at localhost:8080
```
npm run dev
```

<!--
# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
``` -->

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
