var title = "Umsatz";
// color management
// var colorPicker = d3.scaleQuantize()
//     .domain([0, 1])
//     .range(["#005128", "#006532", "#00783b", "#008c45", "#009f4f", "#6aae6a"]);

var colorPicker = ["#005128", "#006532", "#00783b", "#008c45", "#009f4f", "#6aae6a"];

var dataset = (tableData) ? tableData._data.linearChart : undefined ;
var max = d3.max(dataset, function(d){ return d.value; });
var currency = "CHF";
var dest = "#rightLowerGraph";

// add the title
var h4 = document.createElement('h4');
h4.innerText = title;
document.querySelector(dest).append(h4);

// Loop through the elements and render those
for(var i = 0, ln = dataset.length; i < ln; i++) {
  // Captions first
  var div = document.createElement('div');
  div.className = "col-xs-12";
  // left part
  var left = document.createElement("span");
  left.innerText = dataset[i].name+":";
  div.append(left);
  // right part
  var pickedColor = colorPicker[Math.round((dataset[i].value/max)*(colorPicker.length-1))];
  var right = document.createElement("span");
  right.style.float = "right";
  right.innerHTML = currency + " " + "<span style='color:"+pickedColor+"'>"+dataset[i].value+"</span>";
  div.append(right);
  // append
  document.querySelector(dest).append(div);
  // Make an SVG Container
  var svg = d3.select(dest).append("svg")
                                      .attr("width", "100%")
                                      .attr("height", 22);
  // Draw the Rectangle
  var rectangle = svg.append("rect")
                              .attr("x", 0)
                              .attr("y", 0)
                              .attr("rx", 10)
                              .attr("ry", 30)
                              .attr("width", (100*dataset[i].value/max)+"%")
                              .attr("height", 22)
                              .attr("fill", pickedColor);
  //
  document.querySelector(dest).append(document.createElement("br"));
}