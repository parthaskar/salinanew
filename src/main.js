// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import vueResource from 'vue-resource'
import {routes} from './routes'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
// export const bus = new Vue();

Vue.config.productionTip = true

Vue.use(vueResource)
Vue.use(VueRouter)

// Vue.http.options.root = 'http://172.16.1.157:8002';

const router = new VueRouter({
  mode:'history',
  routes
})

Vue.component('icon', Icon)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App}
})
